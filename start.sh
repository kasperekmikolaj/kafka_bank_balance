#!/bin/bash

echo "building application"
mvn clean install

echo "removing previous environment and starting application"
./run.sh -c
