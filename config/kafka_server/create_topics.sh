#!/bin/bash

create_kafka_topic() {
    local TOPIC_NAME="$1"
    local PARTITIONS="$2"
    local REPLICATION="$3"
    local CLEANUP_POLICY="$4"

    if [[ " ${EXISTING_TOPICS[*]} " == *" $TOPIC_NAME "* ]]; then
        echo "$TOPIC_NAME already created"
    else
        echo "$TOPIC_NAME does not exist yet, creating"
        kafka-topics.sh --bootstrap-server kafka:9092 --create --topic $TOPIC_NAME --partitions $PARTITIONS \
        --config cleanup.policy=$CLEANUP_POLICY --replication-factor $REPLICATION
    fi
}

echo creating topics

TOPIC_NAMES=("bank-balance" "account-operations")
CONNECT_OFFSET_TOPIC="connect-offsets"
CONNECT_CONFIG_TOPIC="connect-configs"
CONNECT_STATUS_TOPIC="connect-status"

LISTED_TOPICS=$(kafka-topics.sh --bootstrap-server kafka:9092 --list)
IFS=$'\n' read -rd '' -a EXISTING_TOPICS <<< "$LISTED_TOPICS"

for topic in "${TOPIC_NAMES[@]}"; do
  create_kafka_topic $topic 3 1 "delete"
done

  create_kafka_topic $CONNECT_OFFSET_TOPIC 50 1 "compact"
  create_kafka_topic $CONNECT_CONFIG_TOPIC 1 1 "compact"
  create_kafka_topic $CONNECT_STATUS_TOPIC 5 1 "compact"
