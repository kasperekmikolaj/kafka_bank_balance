#!/bin/bash

echo running custom startup script
echo default entrypoint part
set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purposes

# Load libraries
. /opt/bitnami/scripts/liblog.sh
. /opt/bitnami/scripts/libbitnami.sh
. /opt/bitnami/scripts/libkafka.sh

# Load Kafka environment variables
. /opt/bitnami/scripts/kafka-env.sh

print_welcome_page

info "** Starting Kafka setup **"
/opt/bitnami/scripts/kafka/setup.sh
info "** Kafka setup finished! **"

echo custom part
# run kafka in background
"/opt/bitnami/scripts/kafka/run.sh" &
# get background process pid
PID=$!

# run kafka configuration scripts
/custom_config/create_topics.sh

# do not stop container when kafka is running in background
wait $PID
