package org.example;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;

import java.util.Objects;
import java.util.Properties;

@Slf4j
@Builder
@Getter
@Setter
@AllArgsConstructor
public class KafkaPropertiesBuilder {

    // environment variables
    private final String ENVIRONMENT_TYPE = "ENVIRONMENT_TYPE";

    // basic props
    private final String DOCKER_BOOTSTRAP_SERVERS = "kafka:9092";
    private final String DOCKER_SCHEMA_REGISTRY_URL = "http://kafka-schema-registry:8089";
    private String bootstrapServers = "localhost:9094";
    private String schemaRegistryUrl = "http://localhost:8089";
    private final String SCHEMA_REGISTRY_URL_CONFIG = "schema.registry.url";

    // producer props
    private final String keySerializer = StringSerializer.class.getName();
    private final String valueSerializer = KafkaAvroSerializer.class.getName();
    private final String acksConfig = "all";
    private final String retriesCount = "3";
    private final String enableIdempotence = "true";

    // streams props
    private final String applicationId = "streams-app-id"; // consumer group.id, default client.id prefix, prefix to internal changelog topics
    private final String autoOffsetReset = "earliest"; // consume topic from start
    private final String keySerde = Serdes.StringSerde.class.getName();
    private final String valueSerde = SpecificAvroSerde.class.getName();

    public KafkaPropertiesBuilder() {
        String currentEnv = System.getenv(ENVIRONMENT_TYPE);
        if (Objects.nonNull(currentEnv) && currentEnv.equals("DOCKER")) {
            log.info("using urls for docker environment");
            bootstrapServers = DOCKER_BOOTSTRAP_SERVERS;
            schemaRegistryUrl = DOCKER_SCHEMA_REGISTRY_URL;
        }
    }

    public Properties getProducerProperties() {
        Properties producerProperties = new Properties();
        addBasicProperties(producerProperties);
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
        producerProperties.put(ProducerConfig.ACKS_CONFIG, "all"); // strongest producing guarantee - server + replicas
        producerProperties.put(ProducerConfig.RETRIES_CONFIG, "3");
        producerProperties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        return producerProperties;
    }

    public Properties getStreamsProperties() {
        Properties streamsProperties = new Properties();
        addBasicProperties(streamsProperties);
        streamsProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, keySerde);
        streamsProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, valueSerde);
        streamsProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        streamsProperties.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE_V2);
        return streamsProperties;
    }

    private void addBasicProperties(Properties properties) {
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
    }
}
