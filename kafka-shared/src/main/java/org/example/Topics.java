package org.example;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Topics {
    ACCOUNT_OPERATIONS("account-operations"),
    BANK_BALANCE("bank-balance");

    private final String topicName;
}
