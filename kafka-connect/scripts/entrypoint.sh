#!/bin/bash

connect-distributed.sh /my_files/config/worker.properties &
# get background process pid
PID=$!

# run configuration scripts
/my_files/scripts/add-connectors-to-cluster.sh

# do not stop container when kafka is running in background
wait $PID
