#!/bin/bash

KAFKA_CONNECT_URL="http://kafka-connect:8083/connectors"

# Keep checking the connection to the URL until successful
while ! curl -s -o /dev/null -w "%{http_code}" $KAFKA_CONNECT_URL | grep -q "200"; do
    echo "Failed to connect to $KAFKA_CONNECT_URL. Retrying in 5 seconds..."
    sleep 5
done

echo "starting mongo sink connector"
curl -X POST -H "Content-Type: application/json" --data @/my_files/config/mongo-sink-account-operations-config.json $KAFKA_CONNECT_URL

echo "starting postgres sink connector"
curl -X POST -H "Content-Type: application/json" --data @/my_files/config/postgres-sink-balance.json $KAFKA_CONNECT_URL
