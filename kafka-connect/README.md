if connector files are already downloaded to connector-jars/ directory they will not be overwritten in next builds
if you want to download them again during build process you must manually delete them
