#!/bin/bash

print_help() {
    echo
    echo "Usage: $0 [options]"
    echo "Options:"
    echo "  -h, --help   Display this help message"
    echo "  -c, --clean  Delete containers and volumes"
}

CLEAN=false

# Parse command line options
while [[ $# -gt 0 ]]; do
    case "$1" in
        -c|--clean) CLEAN=true; shift;;
        -h|--help) print_help; exit 0;;
        *) echo "Unknown option: $1" exit 1;;
    esac
done

if $CLEAN; then
    echo "Deleting containers and volumes"
    docker compose -f docker-compose.yml -f docker-compose.ui.yml down --volumes --remove-orphans
else
    docker compose -f docker-compose.yml -f docker-compose.ui.yml down
fi

docker compose -f docker-compose.yml -f docker-compose.ui.yml up
