<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>kafka_bank_balance</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <modules>
        <module>kafka-connect</module>
        <module>kafka-shared</module>
        <module>producer-application</module>
        <module>balance-stream</module>
        <module>schema-registry</module>
    </modules>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- dependencies -->
        <lombok.version>1.18.28</lombok.version>
        <logback.version>1.4.11</logback.version>
        <slf4j.version>2.0.7</slf4j.version>
        <kafka.clients.version>3.5.1</kafka.clients.version>
        <kafka.streams.version>3.5.1</kafka.streams.version>
        <streams.avro.serde.version>7.4.0</streams.avro.serde.version>
        <kafka.avro.serializer.version>7.4.0</kafka.avro.serializer.version>

        <!-- plugins -->
        <dockerfile.maven.version>1.4.13</dockerfile.maven.version>
        <shade.plugin.version>3.5.0</shade.plugin.version>
    </properties>

    <!-- dependencies section -->
    <dependencies>
        <dependency>
            <groupId>io.confluent</groupId>
            <artifactId>kafka-streams-avro-serde</artifactId>
            <version>${streams.avro.serde.version}</version>
        </dependency>
        <!-- for kafka registry avro schema -->
        <dependency>
            <groupId>io.confluent</groupId>
            <artifactId>kafka-avro-serializer</artifactId>
            <version>${kafka.avro.serializer.version}</version>
        </dependency>
        <!-- utils -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>
        <!-- logging -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
    </dependencies>

    <!-- management section -->
    <dependencyManagement>
        <dependencies>
            <!-- custom dependencies -->
            <dependency>
                <groupId>org.example</groupId>
                <artifactId>kafka-shared</artifactId>
                <version>${project.version}</version>
            </dependency>
            <!-- kafka -->
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka-clients</artifactId>
                <version>${kafka.clients.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka-streams</artifactId>
                <version>${kafka.streams.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <version>${shade.plugin.version}</version>
                </plugin>
                
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>dockerfile-maven-plugin</artifactId>
                    <version>${dockerfile.maven.version}</version>
                    <executions>
                        <execution>
                            <id>build-docker-images</id>
                            <goals>
                                <goal>build</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <repository>papaj/kafka/bank-balance/${project.artifactId}</repository>
                        <tag>${project.version}</tag>
                        <buildArgs>
                            <JAR_FILE>target/${project.build.finalName}.jar</JAR_FILE>
                        </buildArgs>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <repositories>
        <repository>
            <id>maven-central</id>
            <url>https://repo.maven.apache.org/maven2</url>
        </repository>
        <repository>
            <id>confluent-repository</id>
            <url>https://packages.confluent.io/maven/</url>
        </repository>
    </repositories>
</project>
