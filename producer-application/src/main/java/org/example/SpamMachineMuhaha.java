package org.example;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Random;

@Slf4j
public class SpamMachineMuhaha {

    private static final List<String> people = Arrays.asList("mareczek", "ziomeczek", "ewka", "pewka");
    private static final Properties producerProperties = new KafkaPropertiesBuilder().getProducerProperties();
    private static final BankBalanceProducer bankBalanceProducer = new BankBalanceProducer(producerProperties);
    private static final Random generator = new Random();


    public static void MAKE_SOME_NOISE() {
        while (true) {
            for (int i = 0; i < 10; i++) {
                for (String person : people) {
                    bankBalanceProducer.send(person, getRandomAmount());
                }
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                log.error("something not right... not right for sure...");
                throw new RuntimeException(e);
            }
        }
    }

    private static int getRandomAmount() {
        return generator.nextInt(1, 10);
    }
}
