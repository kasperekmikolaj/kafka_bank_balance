package org.example;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Properties;

@Slf4j
public class BankBalanceProducer {

    private @Getter
    final String topic = Topics.ACCOUNT_OPERATIONS.getTopicName();
    private final KafkaProducer<String, SpecificRecord> kafkaProducer;

    public BankBalanceProducer(Properties properties) {
        kafkaProducer = new KafkaProducer<>(properties);
    }

    public void send(String name, int amount) {
        SpecificRecord avroRecord = new AccountOperationAvroRecord(name, amount,
                LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        ProducerRecord<String, SpecificRecord> record = new ProducerRecord<>(topic, name, avroRecord);
        kafkaProducer.send(record);
    }

    public void sendNow(String name, int amount) {
        send(name, amount);
        kafkaProducer.flush();
    }
}
