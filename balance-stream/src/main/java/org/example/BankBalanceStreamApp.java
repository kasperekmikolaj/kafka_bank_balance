package org.example;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Properties;

@Slf4j
public class BankBalanceStreamApp {

    private final KafkaStreams streamsApplication;
    private final Serde<BankBalanceAvroRecord> bankBalanceRecordSerde = new SpecificAvroSerde<>();
    private final Serde<AccountOperationAvroRecord> accountOperationSerde = new SpecificAvroSerde<>();

    BankBalanceStreamApp() {
        KafkaPropertiesBuilder kafkaPropertiesBuilder = new KafkaPropertiesBuilder();
        bankBalanceRecordSerde.configure(Collections.singletonMap(
                kafkaPropertiesBuilder.getSCHEMA_REGISTRY_URL_CONFIG(), kafkaPropertiesBuilder.getSchemaRegistryUrl()), false);
        accountOperationSerde.configure(Collections.singletonMap(
                kafkaPropertiesBuilder.getSCHEMA_REGISTRY_URL_CONFIG(), kafkaPropertiesBuilder.getSchemaRegistryUrl()), false);
        Properties streamsProperties = new KafkaPropertiesBuilder().getStreamsProperties();
        StreamsBuilder builder = new StreamsBuilder();

        builder.stream(Topics.ACCOUNT_OPERATIONS.getTopicName(), Consumed.with(Serdes.String(), accountOperationSerde))
                .groupByKey(Grouped.with("key-grouping", Serdes.String(), accountOperationSerde))
                .aggregate(
                        this::getInitialBalanceRecord,
                        (key, nextValue, currentAggregationRes) ->
                                aggregateNextValue(currentAggregationRes, nextValue),
                        Materialized.with(Serdes.String(), bankBalanceRecordSerde)
                )
                .toStream().to(Topics.BANK_BALANCE.getTopicName(), Produced.with(Serdes.String(), bankBalanceRecordSerde));

        Topology topology = builder.build();
        streamsApplication = new KafkaStreams(topology, streamsProperties);
        log.info(streamsApplication.toString());
    }

    public void startApplication() {
        streamsApplication.cleanUp();
        streamsApplication.start();
        Runtime.getRuntime().addShutdownHook(new Thread(streamsApplication::close));
    }

    private BankBalanceAvroRecord aggregateNextValue(BankBalanceAvroRecord currentResult, AccountOperationAvroRecord nextValue) {
        currentResult.setName(nextValue.getName());
        currentResult.setAccountBalance(currentResult.getAccountBalance() + nextValue.getAmount());
        if (currentResult.getLastModificationTime().isAfter(
                nextValue.getModificationTime().atZone(ZoneId.systemDefault()).toInstant())) {
            log.error("most likely we are fucked. lastModificationTime: {}, currentModificationTime: {}, name: {}," +
                            " amount: {}", currentResult.getLastModificationTime(), nextValue.getModificationTime(),
                    currentResult.getName(), nextValue.getAmount());
            throw new RuntimeException("account modifications are not in order");
        }
        currentResult.setLastModificationTime(nextValue.getModificationTime().atZone(ZoneId.systemDefault()).toInstant());
        return currentResult;
    }

    private BankBalanceAvroRecord getInitialBalanceRecord() {
        return new BankBalanceAvroRecord("", 0,
                LocalDateTime.of(1000, 1, 1, 1, 1).atZone(ZoneId.systemDefault()).toInstant());
    }
}
