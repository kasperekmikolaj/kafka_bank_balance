#!/bin/bash
# Copyright VMware, Inc.
# SPDX-License-Identifier: APACHE-2.0

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purposes

# Load libraries
. /opt/bitnami/scripts/libschemaregistry.sh
. /opt/bitnami/scripts/libbitnami.sh
. /opt/bitnami/scripts/liblog.sh

# Load Schema Registry environment variables
. /opt/bitnami/scripts/schema-registry-env.sh

print_welcome_page

info "** Starting Schema Registry setup **"
/opt/bitnami/scripts/schema-registry/setup.sh
info "** Schema Registry setup finished! **"

echo ""
# run registry in background
"/opt/bitnami/scripts/schema-registry/run.sh" &
# get background process pid
PID=$!

# run kafka configuration scripts
/my_files/scripts/create_schemas.sh

# do not stop container when schema registry is running in background
wait $PID
