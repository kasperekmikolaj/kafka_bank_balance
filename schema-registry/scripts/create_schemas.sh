#!/bin/bash

echo "entering creating schemas script"
SCHEMA_URL="http://kafka-schema-registry:8089"
ACCOUNT_OPERATIONS_SCHEMA_URL="http://kafka-schema-registry:8089/subjects/account-operations-value/versions?normalize=true"
BANK_BALANCE_SCHEMA_URL="http://kafka-schema-registry:8089/subjects/bank-balance-value/versions?normalize=true"

# Keep checking the connection to the URL until successful
while true; do
    if curl -s "$SCHEMA_URL" &> /dev/null; then
        # If connection is successful, exit the loop
        break
    else
        echo "Failed to connect to $SCHEMA_URL. Retrying in 5 seconds..."
        sleep 5
    fi
done

echo "creating account operations schema"
curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data @/my_files/schemas/account_operations_record_schema.json $ACCOUNT_OPERATIONS_SCHEMA_URL

echo "creating bank balance schema"
curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data @/my_files/schemas/bank_balance_record_schema.json $BANK_BALANCE_SCHEMA_URL
